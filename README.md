<!DOCTYPE html>
<html>
<head>
    <title>Simple Chat</title>
    <style>
        #chat-box {
            width: 300px;
            height: 300px;
            border: 1px solid #ccc;
            overflow-y: scroll;
        }
    </style>
</head>
<body>
    <div id="chat-box"></div>
    <input type="text" id="message-input" placeholder="Type your message...">
    <button id="send-button">Send</button>

    <script>
        const chatBox = document.getElementById('chat-box');
        const messageInput = document.getElementById('message-input');
        const sendButton = document.getElementById('send-button');

        sendButton.addEventListener('click', sendMessage);

        function sendMessage() {
            const message = messageInput.value;
            if (message.trim() !== '') {
                addMessage('You', message);
                messageInput.value = '';
            }
        }

        function addMessage(sender, message) {
            const messageElement = document.createElement('div');
            messageElement.textContent = `${sender}: ${message}`;
            chatBox.appendChild(messageElement);
        }
    </script>
</body>
</html>
